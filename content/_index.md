## ¡Bienvenid@s!

A mi sitio web donde encontraras todo lo relacionado con el curso de certificación en seguridad informática impartido por **Corenetwoks** y la Comunidad de Madrid.

Al tratarse de una **certificación (IFCT0109)** este curso consta de 5 módulos con una duración de **500 horas** y que dura aproximadamente 5 meses. Los módulos de los que consta son los siguientes:

- Seguridad en equipos informáticos (90 h)
- Auditoría de seguridad informática (90 h)
- Gestión de incidentes de seguridad informática (90 h)
- Sistemas seguros de acceso y transmisión de datos (60 h)
- Gestión de servicios en el sistema informáticos (90 h)
- Prácticas profesionales no laborales (80 h)

Espero que esta web os sirva de mucha ayuda y de guía para que este curso sea lo más productivo posible.

Muchas gracias y hasta pronto.

Su amigo y compañero Randy XD.


